# Hiring Creditoo

## Requirements

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Instalation

1. Clone the project
2. Run **docker-compose up -d** to build the containers
3. Run **docker-compose exec php composer install** to install the dependencies

## Endpoints

### User Information
```bash
curl -X GET http://localhost:8080/api/users/{username}
```

### User Repositories
```bash
curl -X GET http://localhost:8080/api/users/{username}/repos
```

## Testing

With the containers running, run the following command:
```bash
docker-compose exec php composer test
```