<?php

namespace App\Exceptions\Github;

use Exception;

class ConnectionErrorException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(['error' => 'could_not_connect_to_github'], 500);
    }
}
