<?php

namespace App\Http\Controllers;

use App\Services\GithubService;
use App\Transformers\RepositoriesTransformer;
use App\Transformers\UsersTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\JsonResponse;

class UsersController extends Controller
{
    /**
     * The GitHub service.
     *
     * @var GithubService
     */
    private $service;

    /**
     * The controller constructor.
     *
     * @param GithubService $service
     */
    public function __construct(GithubService $service)
    {
        $this->service = $service;
    }

    /**
     * User Information
     * Retrieve user information from GitHub.
     *
     * @param string $username
     * @return JsonResponse
     */
    public function view(string $username) : JsonResponse
    {
        // A key to identify the cache
        $key = sprintf('%s_info', $username);

        // Let's cache the user info for 60 minutes
        $user = Cache::remember($key, 60, function () use ($username) {
            return $this->service->getUserInfo($username);
        });

        // Transform and return the user info in JSON format
        return response()->json(
            (new UsersTransformer)->transform($user)
        );
    }

    /**
     * User Repositories
     * Retrieve user repositories from GitHub.
     *
     * @param string $username
     * @return JsonResponse
     */
    public function repositories(string $username) : JsonResponse
    {
        // A key to identify the cache
        $key = sprintf('%s_repositories', $username);

        // Let's cache the user's repositories for 60 minutes
        $repositories = Cache::remember($key, 60, function () use ($username) {
            return $this->service->getUserRepositories($username);
        });

        // Transform and return the repositories in JSON format
        return response()->json(
            (new RepositoriesTransformer)->transformCollection($repositories)
        );
    }
}
