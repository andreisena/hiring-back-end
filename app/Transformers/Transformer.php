<?php

namespace App\Transformers;

abstract class Transformer
{
    /**
     * Tranform a collection of items.
     *
     * @param array $items
     * @return array
     */
    public function transformCollection($items) : array
    {
        return array_map([$this, 'transform'], $items);
    }

    /**
     * Transform a single item.
     *
     * @param array $item
     * @return array
     */
    abstract public function transform(array $item) : array;
}
