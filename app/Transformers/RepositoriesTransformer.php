<?php

namespace App\Transformers;

class RepositoriesTransformer extends Transformer
{
    /**
     * Transform a single repository.
     *
     * @param array $item
     * @return array
     */
    public function transform(array $item) : array
    {
        return [
            'id' => (int) $item['id'],
            'name' => $item['name'],
            'description' => $item['description'],
            'html_url' => $item['html_url'],
        ];
    }
}
