<?php

namespace App\Transformers;

class UsersTransformer extends Transformer
{
    /**
     * Transform a single user.
     *
     * @param array $item
     * @return array
     */
    public function transform(array $item) : array
    {
        return [
            'id' => (int) $item['id'],
            'login' => $item['login'],
            'name' => $item['name'],
            'avatar_url' => $item['avatar_url'],
            'html_url' => $item['html_url'],
        ];
    }
}
