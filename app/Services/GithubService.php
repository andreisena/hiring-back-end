<?php

namespace App\Services;

use App\Exceptions\Github\ConnectionErrorException;
use App\Exceptions\Github\UserNotFoundException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class GithubService
{
    /**
     * HTTP Handler to be used as a client for the requests.
     */
    private $client;

    /**
     * Class constructor
     */
    public function __construct()
    {
        // Initializing the Guzzle library to make requests on GitHub API
        $this->client = new Client(['base_uri' => 'https://api.github.com/']);
    }

    /**
     * Get user info from GitHub.
     *
     * @param string $username
     * @return mixed
     */
    public function getUserInfo(string $username)
    {
        try {
            $response = $this->client->request('GET', 'users/' . $username);
            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            throw new UserNotFoundException($e->getMessage(), $e->getCode());
        } catch (Exception $e) {
            throw new ConnectionErrorException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * List user repositories from GitHub.
     *
     * @param string $username
     * @return mixed
     */
    public function getUserRepositories(string $username)
    {
        try {
            $response = $this->client->request('GET', 'users/' . $username . '/repos');
            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            throw new UserNotFoundException($e->getMessage(), $e->getCode());
        } catch (Exception $e) {
            throw new ConnectionErrorException($e->getMessage(), $e->getCode());
        }
    }
}
