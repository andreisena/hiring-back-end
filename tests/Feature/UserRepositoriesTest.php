<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRepositoriesTest extends TestCase
{
    /**
     * Test if the user repos endpoint returns a valid JSON.
     *
     * @return void
     */
    public function testSuccessfullyRetriveUserRepositories()
    {
        $response = $this->json('GET', '/api/users/github/repos');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'name',
                    'description',
                    'html_url',
                ]
            ]);
    }
}
