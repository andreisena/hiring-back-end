<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserInfoTest extends TestCase
{
    /**
     * Test if the user info endpoint returns a valid JSON.
     *
     * @return void
     */
    public function testSuccessfullyRetriveUserInfo()
    {
        $response = $this->json('GET', '/api/users/github');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'login',
                'name',
                'avatar_url',
                'html_url',
            ]);
    }
}
